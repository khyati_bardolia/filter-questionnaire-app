import React, { useState, useEffect } from "react";
export const SearchComponent = (props) => {
  const { questions } = props;
  return (
    <>
      <div>
        <ol className="px-4">
          {questions && questions.length ? (
            questions.map((item) => {
              return (
                <li className="text-left" key={item.id}>
                  {item.title}
                </li>
              );
            })
          ) : (
            <div>No Record Found!</div>
          )}
        </ol>
      </div>
    </>
  );
};
