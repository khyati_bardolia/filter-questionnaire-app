import { Fragment } from 'react';

const FilterTags = ({ appliedFilters, resetFilters }) => {
  let filters = [];
  if (Object.keys(appliedFilters).length) {
    const companies = appliedFilters.co ? Object.values(appliedFilters.co) : [];
    const position = appliedFilters.pos
      ? Object.values(appliedFilters.pos)
      : [];
    const title = appliedFilters.q;

    filters = [].concat(companies, position, title);
  }

  return (
    <div className={'d-flex py-3'}>
      {filters &&
        filters?.map((filter, i) => {
          return (
            <Fragment key={i}>
            {filter && <span  className={'filter-tags'}>
              {filter}
            </span>}
            </Fragment>
          );
        })}
      {Object.keys(appliedFilters).length ? (
        <button
          type="button"
          className="btn btn-secondary btn-reset ml-3"
          onClick={resetFilters}
        >
          Reset
        </button>
      ) : null}
    </div>
  );
};
export default FilterTags;
