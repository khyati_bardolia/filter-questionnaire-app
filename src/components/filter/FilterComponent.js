import React, { useState, useEffect, useCallback } from 'react';
import FilterTags from './FilterTags';
import { SearchComponent } from './SearchComponent';
import { useHistory } from 'react-router-dom';
import '../../css/filterPopup.css';
import { Button, Modal, Form } from 'react-bootstrap';

export const FilterComponent = (props) => {
  console.log('props', props);
  const [inputValue, setInputValue] = useState('');
  const [questions, setQuestions] = useState([]);
  const [filteredData, setfilteredData] = useState([]);
  const [show, setShow] = useState(false);
  const [companies, setCompanies] = useState([]);
  const [positions, setPositions] = useState([]);
  const [singleValue, setSingleValue] = useState([]);
  const [companyCheckBoxItems, setCompanyCheckBoxItems] = useState(new Map());
  const [selectedCompanies, setSelectedCompanies] = useState([]);
  const [positionCheckBoxItems, setPositionCheckBoxItems] = useState(new Map());
  const [selectedPositions, setSelectedPositions] = useState([]);
  const [queryParams, setQueryParams] = useState({});
  const history = useHistory();

  console.log('history', history);

  console.log('merge with master');
  const getAllQuestions = async () => {
    const questionData = await fetch(
      'https://60081f78309f8b0017ee5615.mockapi.io/api/v1/questions'
    )
      .then((response) => response.json())
      .catch((error) => console.log(error));
    setQuestions(questionData);
    setfilteredData(questionData);
    return questionData;
  };

  useEffect(() => {
    getAllQuestions();
    console.log('questions', questions);
  }, []);

  const handleChange = (e) => {
    setInputValue(e.target.value);
  };
  const openFilterPopup = () => {
    setShow(!show);
  };
  console.log('inputValue', inputValue);
  const searchData = (e) => {
    console.log('calling everytime');
    const {
      location: { pathname, search },
    } = history;

    let params = new URLSearchParams(search).toString();
    console.log('params', JSON.parse(JSON.stringify(params)));

    if (e.keyCode === 13) {
      setfilteredData(filterArray());

      const obj = { q: inputValue };
      setQueryParams((prevState) => {
        return Object.assign(prevState, obj);
      });
    }
  };

  const uniqueArray = (item, pos, self) => {
    return self.indexOf(item) === pos;
  };

  const handleClose = () => setShow(false);

  const handleShow = () => {
    setShow(true);
    const allCompanies = [];
    const allPositions = [];
    questions &&
      questions.length &&
      questions.map((data) => {
        data.companies.map((name) => {
          return allCompanies.push(name);
        });
        data.positions.map((name) => {
          return allPositions.push(name);
        });
      });
    setCompanies(allCompanies.filter(uniqueArray));
    setPositions(allPositions.filter(uniqueArray));
  };

  const onlyUnique = (value, index, self) => {
    return self.indexOf(value) === index;
  };

  const selectCompanies = useCallback(({ target: { name, checked } }) => {
    setCompanyCheckBoxItems((prevState) => {
      return new Map(prevState).set(name, checked);
    });
  }, []);

  useEffect(() => {
    const finalSelected = [];
    console.log('------------companyCheckBoxItems-------------');
    console.log(companyCheckBoxItems);
    companyCheckBoxItems.forEach((key, value) => {
      if (key) {
        finalSelected.push(value);
      }
      setSelectedCompanies(finalSelected);
    });
  }, [companyCheckBoxItems]);
  console.log('selectedcc', selectedCompanies);

  const selectPositions = useCallback(({ target: { name, checked } }) => {
    setPositionCheckBoxItems((prevState) => {
      return new Map(prevState).set(name, checked);
    });
  }, []);

  useEffect(() => {
    const positionsArr = [];
    positionCheckBoxItems.forEach((key, value) => {
      if (key) {
        positionsArr.push(value);
      }
      setSelectedPositions(positionsArr);
    });
  }, [positionCheckBoxItems]);
  console.log('selectedpp', selectedPositions);


  useEffect(() => {
    const {
      location: { pathname },
    } = history;

    if (queryParams) {
      if (!queryParams.q) {
        delete queryParams.q;
      }

      if (!(queryParams.co || []).length) {
        delete queryParams.co;
      }

      if (!(queryParams.pos || []).length) {
        delete queryParams.pos;
      }
    }

    history.push({
      pathname: pathname,
      search: Object.keys(queryParams).length
        ? '?' + new URLSearchParams(queryParams).toString()
        : '',
    });
  }, [history, Object.values(queryParams)]);

  console.log('queryParams121212121212', queryParams);
  const applyFilters = () => {
    console.log('apply filter called');
    const params = {
      co: selectedCompanies,
      pos: selectedPositions,
    };

    setQueryParams((prevState) => {
      return Object.assign(prevState, params);
    });

    handleClose();
    setfilteredData(filterArray());
  };
  const getValue = (value) =>
    typeof value === 'string' ? value.toUpperCase() : value;

  const filterArray = () => {
    const array =
      questions &&
      questions.length &&
      questions.filter((item) => {
        return (
          item &&
          (item.title || '')
            .toLowerCase()
            .includes((inputValue || '').toLowerCase())
        );
      });

    const filters = {
      companies: selectedCompanies && selectedCompanies,
      positions: selectedPositions && selectedPositions,
    };

    const filterKeys = Object.keys(filters);

    return array.filter((eachObj) => {
      return filterKeys.every((eachKey) => {
        console.log('filters[eachKey]', filters[eachKey]);
        if (!filters[eachKey].length) {
          return true; // passing an empty filter means that filter is ignored.
        }
        console.log('eachObj[eachKey]', eachObj[eachKey]);
        console.log('result-->>', filters[eachKey].includes(eachObj[eachKey]));
        return filters[eachKey].some((i) => eachObj[eachKey].includes(i));
      });
    });
  };

  console.log('final dataaaaa', filteredData);

  const resetFilters = () => {
    setQueryParams({});
    setSelectedCompanies([]);
    setSelectedPositions([]);
    getAllQuestions();
  };

  return (
    <>
      <div className="container">
        <div className="py-2">
          <h2>Get Your Question Bank!</h2>
        </div>
        <div className="py-2 d-flex w-100">
          <div className="w-75 pr-2">
            <input
              onChange={handleChange}
              className="form-control"
              id="myInput"
              type="text"
              placeholder="Search.."
              onKeyUp={searchData}
            />
          </div>
          <div className="w-25 d-flex" onClick={handleShow}>
            <Button variant="primary">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="20"
                height="20"
                fill="currentColor"
                className="bi bi-filter"
                viewBox="0 0 16 16"
              >
                <path d="M6 10.5a.5.5 0 0 1 .5-.5h3a.5.5 0 0 1 0 1h-3a.5.5 0 0 1-.5-.5zm-2-3a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7a.5.5 0 0 1-.5-.5zm-2-3a.5.5 0 0 1 .5-.5h11a.5.5 0 0 1 0 1h-11a.5.5 0 0 1-.5-.5z" />
              </svg>
              Advanced
            </Button>
          </div>
        </div>
        <div>
          <FilterTags
            appliedFilters={queryParams}
            resetFilters={resetFilters}
          />
        </div>
        <div className="py-2">
          <SearchComponent questions={filteredData} />
        </div>
        <div>
          <Modal show={show} onHide={handleClose}>
            <Modal.Header closeButton>
              <Modal.Title>Select Filters</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <Form className="d-flex flex-row">
                <div className="w-50 d-flex flex-column">
                  <Form.Label>
                    <strong>Companies:</strong>
                  </Form.Label>
                  <Form.Group id="formGridCheckbox">
                    {companies &&
                      companies.length &&
                      companies.map((name, i) => (
                        <Form.Check
                          key={i}
                          checked={selectedCompanies.includes(name)}
                          name={name}
                          id={name}
                          onChange={(e) => selectCompanies(e)}
                          className="pr-2 d-flex align-items-center"
                          type="checkbox"
                          label={name}
                        />
                      ))}
                  </Form.Group>
                </div>
                <div className="w-50 d-flex flex-column">
                  <Form.Label>
                    <strong>Positions:</strong>
                  </Form.Label>
                  <Form.Group id="formGridCheckbox">
                    {positions &&
                      positions.length &&
                      positions.map((name, i) => (
                        <Form.Check
                          key={i}
                          name={name}
                          id={name}
                          checked={selectedPositions.includes(name)}
                          onChange={(e) => selectPositions(e)}
                          className="pr-2 d-flex align-items-center"
                          type="checkbox"
                          label={name}
                        />
                      ))}
                  </Form.Group>
                </div>
              </Form>
            </Modal.Body>
            <Modal.Footer>
              <Button variant="secondary" onClick={handleClose}>
                Cancel
              </Button>
              <Button variant="primary" onClick={applyFilters}>
                Apply
              </Button>
            </Modal.Footer>
          </Modal>
        </div>
      </div>
    </>
  );
};
