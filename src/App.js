import React from 'react';
import './App.css';
import "bootstrap/dist/css/bootstrap.min.css";
import { FilterComponent } from "./components/filter/FilterComponent";
import { BrowserRouter as Router } from "react-router-dom";

function App() {
  return (
    <div className="App">
      <Router>
        <FilterComponent />
      </Router>
    </div>
  );
}

export default App;
